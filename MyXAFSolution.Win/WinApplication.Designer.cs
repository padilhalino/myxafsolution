﻿namespace MyXAFSolution.Win {
    partial class MyXAFSolutionWindowsFormsApplication {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
      this.module1 = new DevExpress.ExpressApp.SystemModule.SystemModule();
      this.module2 = new DevExpress.ExpressApp.Win.SystemModule.SystemWindowsFormsModule();
      this.module3 = new MyXAFSolution.Module.MyXAFSolutionModule();
      this.module4 = new MyXAFSolution.Module.Win.MyXAFSolutionWindowsFormsModule();
      this.securityModule1 = new DevExpress.ExpressApp.Security.SecurityModule();
      this.securityStrategyComplex1 = new DevExpress.ExpressApp.Security.SecurityStrategyComplex();
      this.objectsModule = new DevExpress.ExpressApp.Objects.BusinessClassLibraryCustomizationModule();
      this.validationModule = new DevExpress.ExpressApp.Validation.ValidationModule();
      this.validationWindowsFormsModule = new DevExpress.ExpressApp.Validation.Win.ValidationWindowsFormsModule();
      this.viewVariantsModule1 = new DevExpress.ExpressApp.ViewVariantsModule.ViewVariantsModule();
      this.conditionalAppearanceModule1 = new DevExpress.ExpressApp.ConditionalAppearance.ConditionalAppearanceModule();
      this.reportsModuleV21 = new DevExpress.ExpressApp.ReportsV2.ReportsModuleV2();
      this.fileAttachmentsWindowsFormsModule1 = new DevExpress.ExpressApp.FileAttachments.Win.FileAttachmentsWindowsFormsModule();
      this.pivotChartModuleBase1 = new DevExpress.ExpressApp.PivotChart.PivotChartModuleBase();
      this.pivotChartWindowsFormsModule1 = new DevExpress.ExpressApp.PivotChart.Win.PivotChartWindowsFormsModule();
      this.reportsWindowsFormsModuleV21 = new DevExpress.ExpressApp.ReportsV2.Win.ReportsWindowsFormsModuleV2();
      this.schedulerModuleBase1 = new DevExpress.ExpressApp.Scheduler.SchedulerModuleBase();
      this.schedulerWindowsFormsModule1 = new DevExpress.ExpressApp.Scheduler.Win.SchedulerWindowsFormsModule();
      this.authenticationStandard1 = new DevExpress.ExpressApp.Security.AuthenticationStandard();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      // 
      // securityStrategyComplex1
      // 
      this.securityStrategyComplex1.Authentication = this.authenticationStandard1;
      this.securityStrategyComplex1.RoleType = typeof(DevExpress.Persistent.BaseImpl.EF.PermissionPolicy.PermissionPolicyRole);
      this.securityStrategyComplex1.SupportNavigationPermissionsForTypes = false;
      this.securityStrategyComplex1.UserType = typeof(DevExpress.Persistent.BaseImpl.EF.PermissionPolicy.PermissionPolicyUser);
      // 
      // validationModule
      // 
      this.validationModule.AllowValidationDetailsAccess = true;
      this.validationModule.IgnoreWarningAndInformationRules = false;
      // 
      // reportsModuleV21
      // 
      this.reportsModuleV21.EnableInplaceReports = true;
      this.reportsModuleV21.ReportDataType = typeof(DevExpress.Persistent.BaseImpl.EF.ReportDataV2);
      // 
      // pivotChartModuleBase1
      // 
      this.pivotChartModuleBase1.DataAccessMode = DevExpress.ExpressApp.CollectionSourceDataAccessMode.Client;
      this.pivotChartModuleBase1.ShowAdditionalNavigation = false;
      // 
      // authenticationStandard1
      // 
      this.authenticationStandard1.LogonParametersType = typeof(DevExpress.ExpressApp.Security.AuthenticationStandardLogonParameters);
      // 
      // MyXAFSolutionWindowsFormsApplication
      // 
      this.ApplicationName = "MyXAFSolution";
      this.CheckCompatibilityType = DevExpress.ExpressApp.CheckCompatibilityType.DatabaseSchema;
      this.Modules.Add(this.module1);
      this.Modules.Add(this.module2);
      this.Modules.Add(this.objectsModule);
      this.Modules.Add(this.validationModule);
      this.Modules.Add(this.viewVariantsModule1);
      this.Modules.Add(this.conditionalAppearanceModule1);
      this.Modules.Add(this.reportsModuleV21);
      this.Modules.Add(this.module3);
      this.Modules.Add(this.validationWindowsFormsModule);
      this.Modules.Add(this.fileAttachmentsWindowsFormsModule1);
      this.Modules.Add(this.pivotChartModuleBase1);
      this.Modules.Add(this.pivotChartWindowsFormsModule1);
      this.Modules.Add(this.reportsWindowsFormsModuleV21);
      this.Modules.Add(this.schedulerModuleBase1);
      this.Modules.Add(this.schedulerWindowsFormsModule1);
      this.Modules.Add(this.module4);
      this.Modules.Add(this.securityModule1);
      this.Security = this.securityStrategyComplex1;
      this.UseOldTemplates = false;
      this.DatabaseVersionMismatch += new System.EventHandler<DevExpress.ExpressApp.DatabaseVersionMismatchEventArgs>(this.MyXAFSolutionWindowsFormsApplication_DatabaseVersionMismatch);
      this.CustomizeLanguagesList += new System.EventHandler<DevExpress.ExpressApp.CustomizeLanguagesListEventArgs>(this.MyXAFSolutionWindowsFormsApplication_CustomizeLanguagesList);
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.ExpressApp.SystemModule.SystemModule module1;
        private DevExpress.ExpressApp.Win.SystemModule.SystemWindowsFormsModule module2;
        private MyXAFSolution.Module.MyXAFSolutionModule module3;
        private MyXAFSolution.Module.Win.MyXAFSolutionWindowsFormsModule module4;
        private DevExpress.ExpressApp.Security.SecurityModule securityModule1;
        private DevExpress.ExpressApp.Security.SecurityStrategyComplex securityStrategyComplex1;
        private DevExpress.ExpressApp.Objects.BusinessClassLibraryCustomizationModule objectsModule;
        private DevExpress.ExpressApp.Validation.ValidationModule validationModule;
        private DevExpress.ExpressApp.Validation.Win.ValidationWindowsFormsModule validationWindowsFormsModule;
    private DevExpress.ExpressApp.ViewVariantsModule.ViewVariantsModule viewVariantsModule1;
    private DevExpress.ExpressApp.ConditionalAppearance.ConditionalAppearanceModule conditionalAppearanceModule1;
    private DevExpress.ExpressApp.ReportsV2.ReportsModuleV2 reportsModuleV21;
    private DevExpress.ExpressApp.FileAttachments.Win.FileAttachmentsWindowsFormsModule fileAttachmentsWindowsFormsModule1;
    private DevExpress.ExpressApp.PivotChart.PivotChartModuleBase pivotChartModuleBase1;
    private DevExpress.ExpressApp.PivotChart.Win.PivotChartWindowsFormsModule pivotChartWindowsFormsModule1;
    private DevExpress.ExpressApp.ReportsV2.Win.ReportsWindowsFormsModuleV2 reportsWindowsFormsModuleV21;
    private DevExpress.ExpressApp.Security.AuthenticationStandard authenticationStandard1;
    private DevExpress.ExpressApp.Scheduler.SchedulerModuleBase schedulerModuleBase1;
    private DevExpress.ExpressApp.Scheduler.Win.SchedulerWindowsFormsModule schedulerWindowsFormsModule1;
  }
}
