﻿using DevExpress.Persistent.Base;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyXAFSolution.Module.BusinessObjects
{
  [DefaultClassOptions, ImageName("BO_SaleItem")]
  public class Payment
  {
    [Browsable(false)]
    public Int32 ID { get; protected set; }
    public double Rate { get; set; }
    public double Hours { get; set; }

    [NotMapped]
    public double Amount
    {
      get { return Rate * Hours; }
    }
  }
}
