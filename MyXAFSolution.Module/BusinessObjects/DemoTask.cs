﻿using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl.EF;
using System;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace MyXAFSolution.Module.BusinessObjects
{
  [DefaultClassOptions]
  [ModelDefault("Caption", "Task")]
  [Appearance("FontColorRed", AppearanceItemType = "ViewItem", TargetItems = "*", Context = "ListView",
    Criteria = "Status!='Completed'", FontColor = "Red")]
  public class DemoTask : Task
  {
    public DemoTask() : base()
    {
      TrackedBy = new List<Contact>();
      Priority = Priority.Normal;
    }

    public virtual IList<Contact> TrackedBy { get; set; }

    [Appearance("PriorityBackColorPink", AppearanceItemType = "ViewItem", Context = "Any",
        Criteria = "Priority=2", BackColor = "255, 240, 240")]
    public Priority Priority { get; set; }

    [Action(ToolTip = "Postpone the task to the next day")]
    public void Postpone()
    {
      if (DueDate == DateTime.MinValue)
      {
        DueDate = DateTime.Now;
      }
      DueDate = DueDate + TimeSpan.FromDays(1);
    }

    public bool UpdateBase01 { get; set; }
  }

  public enum Priority
  {
    [ImageName("State_Priority_Low")]
    Low = 0,
    [ImageName("State_Priority_Normal")]
    Normal = 1,
    [ImageName("State_Priority_High")]
    High = 2
  }
}
