﻿using System;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.EF;
using DevExpress.Persistent.BaseImpl.EF;
using DevExpress.Persistent.BaseImpl.EF.PermissionPolicy;
using MyXAFSolution.Module.BusinessObjects;

namespace MyXAFSolution.Module.DatabaseUpdate
{
  // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppUpdatingModuleUpdatertopic.aspx
  public class Updater : ModuleUpdater
  {
    public Updater(IObjectSpace objectSpace, Version currentDBVersion) :
        base(objectSpace, currentDBVersion)
    {
    }

    public override void UpdateDatabaseAfterUpdateSchema()
    {
      base.UpdateDatabaseAfterUpdateSchema();

      Position posManager = ObjectSpace.FindObject<Position>(CriteriaOperator.Parse("Title == 'Manager'"));
      if (posManager == null)
      {
        posManager = ObjectSpace.CreateObject<Position>();
        posManager.Title = "Manager";
      }

      Position posIntern = ObjectSpace.FindObject<Position>(CriteriaOperator.Parse("Title == 'Intern'"));
      if (posIntern == null)
      {
        posIntern = ObjectSpace.CreateObject<Position>();
        posIntern.Title = "Intern";
      }

      Position posDeveloper = ObjectSpace.FindObject<Position>(CriteriaOperator.Parse("Title == 'Developer'"));
      if (posDeveloper == null)
      {
        posDeveloper = ObjectSpace.CreateObject<Position>();
        posDeveloper.Title = "Developer";
      }

      Position posCaretaker = ObjectSpace.FindObject<Position>(CriteriaOperator.Parse("Title == 'Caretaker'"));
      if (posCaretaker == null)
      {
        posCaretaker = ObjectSpace.CreateObject<Position>();
        posCaretaker.Title = "Caretaker";
      }

      Position posCleaningAssistant = ObjectSpace.FindObject<Position>(CriteriaOperator.Parse("Title == 'Cleaning Assistant'"));
      if (posCleaningAssistant == null)
      {
        posCleaningAssistant = ObjectSpace.CreateObject<Position>();
        posCleaningAssistant.Title = "Cleaning Assistant";
      }


      Department depZeladoria = ObjectSpace.FindObject<Department>(CriteriaOperator.Parse("Title == 'Zeladoria' && Office == 'Dep 1'"));
      if (depZeladoria == null)
      {
        depZeladoria = ObjectSpace.CreateObject<Department>();
        depZeladoria.Title = "Zeladoria";
        depZeladoria.Office = "Dep 1";

        depZeladoria.Positions.Add(posCaretaker);
        depZeladoria.Positions.Add(posCleaningAssistant);
      }

      Department depBI = ObjectSpace.FindObject<Department>(CriteriaOperator.Parse("Title == 'BI' && Office == 'Dep 2'"));
      if (depBI == null)
      {
        depBI = ObjectSpace.CreateObject<Department>();
        depBI.Title = "BI";
        depBI.Office = "Dep 2";

        depBI.Positions.Add(posIntern);
        depBI.Positions.Add(posManager);
      }

      Department depDSV = ObjectSpace.FindObject<Department>(CriteriaOperator.Parse("Title == 'DSV' && Office == 'Dep 3'"));
      if (depDSV == null)
      {
        depDSV = ObjectSpace.CreateObject<Department>();
        depDSV.Title = "DSV";
        depDSV.Office = "Dep 3";

        depDSV.Positions.Add(posIntern);
        depDSV.Positions.Add(posDeveloper);
        depDSV.Positions.Add(posManager);
      }

      Department depSUP = ObjectSpace.FindObject<Department>(CriteriaOperator.Parse("Title == 'SUP' && Office == 'Dep 4'"));
      if (depSUP == null)
      {
        depSUP = ObjectSpace.CreateObject<Department>();
        depSUP.Title = "SUP";
        depSUP.Office = "Dep 4";
      }


      Contact contactMary = ObjectSpace.FindObject<Contact>(CriteriaOperator.Parse("FirstName == 'Mary' && LastName == 'Tellitson'"));
      if (contactMary == null)
      {
        contactMary = ObjectSpace.CreateObject<Contact>();
        contactMary.FirstName = "Mary";
        contactMary.LastName = "Tellitson";
        contactMary.Email = "tellitson@example.com";
        contactMary.Birthday = new DateTime(1980, 11, 27);
        contactMary.Anniversary = new DateTime(1980, 11, 27);
        contactMary.Position = posManager;
        contactMary.Department = depBI;
        contactMary.PhoneNumbers.Add(new PhoneNumber() { Number = "41980808080", PhoneType = "Cel" });
        contactMary.PhoneNumbers.Add(new PhoneNumber() { Number = "4130303030", PhoneType = "Home" });
      }

      Contact contact2 = ObjectSpace.FindObject<Contact>(CriteriaOperator.Parse("FirstName == 'Mano' && LastName == 'Brau'"));
      if (contact2 == null)
      {
        contact2 = ObjectSpace.CreateObject<Contact>();
        contact2.FirstName = "Mano";
        contact2.LastName = "Brau";
        contact2.Email = "manobrau@example.com";
        contact2.Birthday = new DateTime(1980, 11, 27);
        contact2.Department = depZeladoria;
        contact2.Position = posCaretaker;
      }

      Contact contact3 = ObjectSpace.FindObject<Contact>(CriteriaOperator.Parse("FirstName == 'Tio' && LastName == 'Feel'"));
      if (contact3 == null)
      {
        contact3 = ObjectSpace.CreateObject<Contact>();
        contact3.FirstName = "Tio";
        contact3.LastName = "Feel";
        contact3.Email = "tiofeel@example.com";
        contact3.Birthday = new DateTime(1980, 11, 27);
        contact3.Department = depZeladoria;
        contact3.Position = posCleaningAssistant;
      }

      Contact contact4 = ObjectSpace.FindObject<Contact>(CriteriaOperator.Parse("FirstName == 'Tia' && LastName == 'Zumira'"));
      if (contact4 == null)
      {
        contact4 = ObjectSpace.CreateObject<Contact>();
        contact4.FirstName = "Tia";
        contact4.LastName = "Zumira";
        contact4.Email = "tiazumira@example.com";
        contact4.Birthday = new DateTime(1980, 11, 27);
        contact4.Department = depZeladoria;
        contact4.Position = posCleaningAssistant;
      }


      DemoTask task1 = ObjectSpace.FindObject<DemoTask>(CriteriaOperator.Parse("Subject == 'Task 1'"));
      if (task1 == null)
      {
        task1 = ObjectSpace.CreateObject<DemoTask>();
        task1.Subject = "Task 1";
        task1.StartDate = DateTime.Now.Date.AddDays(-1);
        task1.TrackedBy.Add(contactMary);
        task1.DueDate = DateTime.Now.Date.AddDays(10);
        task1.Status = DevExpress.Persistent.Base.General.TaskStatus.InProgress;
        task1.MarkCompleted(); // task1.DateCompleted = DateTime.Now;
        task1.Description = "Description aaa " + Environment.NewLine + "Description aab";
        task1.Priority = BusinessObjects.Priority.Normal;
      }

      DemoTask task2 = ObjectSpace.FindObject<DemoTask>(CriteriaOperator.Parse("Subject == 'Task 2'"));
      if (task2 == null)
      {
        task2 = ObjectSpace.CreateObject<DemoTask>();
        task2.Subject = "Task 2";
        task2.StartDate = DateTime.Now.Date.AddDays(1);
        task2.TrackedBy.Add(contactMary);
        task2.DueDate = DateTime.Now.Date.AddDays(11);
        task2.Status = DevExpress.Persistent.Base.General.TaskStatus.InProgress;
        task2.Priority = BusinessObjects.Priority.High;
      }

      DemoTask task3 = ObjectSpace.FindObject<DemoTask>(CriteriaOperator.Parse("Subject == 'Task 3'"));
      if (task3 == null)
      {
        task3 = ObjectSpace.CreateObject<DemoTask>();
        task3.Subject = "Task 3";
        task3.StartDate = DateTime.Now.Date.AddDays(1);
        task3.TrackedBy.Add(contact2);
        task3.Description = "Description ccc " + Environment.NewLine + "Description ccd" + Environment.NewLine + "Description cde";
        task3.Priority = BusinessObjects.Priority.Low;
      }

      // Standard Security
      CreateStandardRole();

      // AD Security
      CreateDefaultRole();
      CreateADRole();

      ObjectSpace.CommitChanges(); //This line persists created object(s).
    }

    private void CreateStandardRole()
    {
      PermissionPolicyRole adminRole = ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", SecurityStrategy.AdministratorRoleName));
      if (adminRole == null)
      {
        adminRole = ObjectSpace.CreateObject<PermissionPolicyRole>();
        adminRole.Name = SecurityStrategy.AdministratorRoleName;
        adminRole.IsAdministrative = true;
      }

      PermissionPolicyRole userRole = ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", "Users"));
      if (userRole == null)
      {
        userRole = ObjectSpace.CreateObject<PermissionPolicyRole>();
        userRole.Name = "Users";
        userRole.PermissionPolicy = SecurityPermissionPolicy.AllowAllByDefault;
        userRole.AddTypePermission<PermissionPolicyRole>(SecurityOperations.FullAccess, SecurityPermissionState.Deny);
        userRole.AddTypePermission<PermissionPolicyUser>(SecurityOperations.FullAccess, SecurityPermissionState.Deny);
        userRole.AddObjectPermission<PermissionPolicyUser>(SecurityOperations.ReadOnlyAccess, "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
        userRole.AddMemberPermission<PermissionPolicyUser>(SecurityOperations.Write, "ChangePasswordOnFirstLogon", null, SecurityPermissionState.Allow);
        userRole.AddMemberPermission<PermissionPolicyUser>(SecurityOperations.Write, "StoredPassword", null, SecurityPermissionState.Allow);
        userRole.AddTypePermission<PermissionPolicyRole>(SecurityOperations.Read, SecurityPermissionState.Allow);
        userRole.AddTypePermission<PermissionPolicyTypePermissionObject>("Write;Delete;Navigate;Create", SecurityPermissionState.Deny);
        userRole.AddTypePermission<PermissionPolicyMemberPermissionsObject>("Write;Delete;Navigate;Create", SecurityPermissionState.Deny);
        userRole.AddTypePermission<PermissionPolicyObjectPermissionsObject>("Write;Delete;Navigate;Create", SecurityPermissionState.Deny);
      }

      // If a user named 'Sam' does not exist in the database, create this user. 
      PermissionPolicyUser user1 = ObjectSpace.FindObject<PermissionPolicyUser>(new BinaryOperator("UserName", "Sam"));
      if (user1 == null)
      {
        user1 = ObjectSpace.CreateObject<PermissionPolicyUser>();
        user1.UserName = "Sam";
        // Set a password if the standard authentication type is used. 
        user1.SetPassword("");
        user1.Roles.Add(adminRole);
      }

      // If a user named 'John' does not exist in the database, create this user. 
      PermissionPolicyUser user2 = ObjectSpace.FindObject<PermissionPolicyUser>(new BinaryOperator("UserName", "John"));
      if (user2 == null)
      {
        user2 = ObjectSpace.CreateObject<PermissionPolicyUser>();
        user2.UserName = "John";
        // Set a password if the standard authentication type is used. 
        user2.SetPassword("");
        user2.Roles.Add(userRole);
      }
    }

    private void CreateADRole()
    {
      PermissionPolicyUser userAdmin = ObjectSpace.FindObject<PermissionPolicyUser>(new BinaryOperator("UserName", System.Security.Principal.WindowsIdentity.GetCurrent().Name));
      if (userAdmin == null)
      {
        userAdmin = ObjectSpace.CreateObject<PermissionPolicyUser>();
        userAdmin.UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
      }
      // If a role with the Administrators name doesn't exist in the database, create this role
      PermissionPolicyRole adminRole = ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", "Administrators"));
      if (adminRole == null)
      {
        adminRole = ObjectSpace.CreateObject<PermissionPolicyRole>();
        adminRole.Name = "Administrators";
      }
      adminRole.IsAdministrative = true;
      userAdmin.Roles.Add(adminRole);
    }

    public override void UpdateDatabaseBeforeUpdateSchema()
    {
      base.UpdateDatabaseBeforeUpdateSchema();
    }

    private PermissionPolicyRole CreateDefaultRole()
    {
      PermissionPolicyRole defaultRole = ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", "Default"));
      if (defaultRole == null)
      {
        defaultRole = ObjectSpace.CreateObject<PermissionPolicyRole>();
        defaultRole.Name = "Default";

        defaultRole.AddObjectPermission<PermissionPolicyUser>(SecurityOperations.Read, "[ID] = CurrentUserId()", SecurityPermissionState.Allow);
        defaultRole.AddNavigationPermission(@"Application/NavigationItems/Items/Default/Items/MyDetails", SecurityPermissionState.Allow);
        defaultRole.AddMemberPermission<PermissionPolicyUser>(SecurityOperations.Write, "ChangePasswordOnFirstLogon", "[ID] = CurrentUserId()", SecurityPermissionState.Allow);
        defaultRole.AddMemberPermission<PermissionPolicyUser>(SecurityOperations.Write, "StoredPassword", "[ID] = CurrentUserId()", SecurityPermissionState.Allow);
        defaultRole.AddTypePermissionsRecursively<PermissionPolicyRole>(SecurityOperations.Read, SecurityPermissionState.Deny);
        defaultRole.AddTypePermissionsRecursively<ModelDifference>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
        defaultRole.AddTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
      }
      return defaultRole;
    }
  }
}
