﻿using System;
using System.Text;
using System.Linq;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Model.Core;
using DevExpress.ExpressApp.Model.DomainLogics;
using DevExpress.ExpressApp.Model.NodeGenerators;
using System.Data.Entity;
using MyXAFSolution.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.ReportsV2;
using MyXAFSolution.Module.Reports;

namespace MyXAFSolution.Module
{
  // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppModuleBasetopic.aspx.
  public sealed partial class MyXAFSolutionModule : ModuleBase
  {
    static MyXAFSolutionModule()
    {
      DevExpress.Data.Linq.CriteriaToEFExpressionConverter.SqlFunctionsType = typeof(System.Data.Entity.SqlServer.SqlFunctions);
      DevExpress.Data.Linq.CriteriaToEFExpressionConverter.EntityFunctionsType = typeof(System.Data.Entity.DbFunctions);
      DevExpress.ExpressApp.SystemModule.ResetViewSettingsController.DefaultAllowRecreateView = false;
      // Uncomment this code to delete and recreate the database each time the data model has changed.
      // Do not use this code in a production environment to avoid data loss.
#if DEBUG
      Database.SetInitializer(new DropCreateDatabaseIfModelChanges<MyXAFSolutionDbContext>());
#endif
    }
    public MyXAFSolutionModule()
    {
      InitializeComponent();
      EnumProcessingHelper.RegisterEnum(typeof(MyXAFSolution.Module.BusinessObjects.Priority));
      DevExpress.ExpressApp.Security.SecurityModule.UsedExportedTypes = DevExpress.Persistent.Base.UsedExportedTypes.Custom;
    }
    public override IEnumerable<ModuleUpdater> GetModuleUpdaters(IObjectSpace objectSpace, Version versionFromDB)
    {
      ModuleUpdater updater = new DatabaseUpdate.Updater(objectSpace, versionFromDB);
      PredefinedReportsUpdater predefinedReportsUpdater = new PredefinedReportsUpdater(Application, objectSpace, versionFromDB);
      predefinedReportsUpdater.AddPredefinedReport<ContactsReport>("Contacts Report", typeof(Contact));
      return new ModuleUpdater[] { updater, predefinedReportsUpdater };
    }
    public override void Setup(XafApplication application)
    {
      base.Setup(application);
      // Manage various aspects of the application UI and behavior at the module level.
    }
  }
}
